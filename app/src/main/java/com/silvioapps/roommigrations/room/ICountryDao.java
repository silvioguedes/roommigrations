package com.silvioapps.roommigrations.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import com.silvioapps.roommigrations.models.Country;
import java.util.List;

@Dao
public interface ICountryDao {
    @Query("SELECT * FROM Country")
    List<Country> getAll();
}