package com.silvioapps.roommigrations.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import com.silvioapps.roommigrations.models.ContinentWithCountries;
import java.util.List;

@Dao
public interface IContinentDao {
    @Query("SELECT * FROM Continent")
    List<ContinentWithCountries> getAllContinentsWithCountries();
}