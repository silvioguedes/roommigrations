package com.silvioapps.roommigrations.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import com.silvioapps.roommigrations.models.Continent;
import com.silvioapps.roommigrations.models.Country;
import com.silvioapps.roommigrations.sqlite.SQLiteDB;

@Database(entities = {Country.class, Continent.class}, version = SQLiteDB.SQLITE_VERSION + 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance = null;

    public static AppDatabase getInstance(Context context){
        if(instance == null){
            final Migration MIGRATION_1_2 = new Migration(SQLiteDB.SQLITE_VERSION, SQLiteDB.SQLITE_VERSION + 1) {
                @Override
                public void migrate(SupportSQLiteDatabase database) {
                    //Necessary to create relations between tables
                    database.execSQL("PRAGMA foreign_keys = ON");

                    // 1. Create new table
                    database.execSQL("CREATE TABLE Continent_TEMP " +
                            "(" +
                            "'continentId' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                            "'continentCode' TEXT, " +
                            "'continentName' TEXT " +
                            ")");

                    // 2. Copy the data
                    database.execSQL("INSERT INTO Continent_TEMP SELECT * FROM Continent");

                    // 3. Remove the old table
                    database.execSQL("DROP TABLE Continent");

                    // 4. Change the table name to the correct one
                    database.execSQL("ALTER TABLE Continent_TEMP RENAME TO Continent");

                    // 1. Create new table
                    database.execSQL(" CREATE TABLE Country_TEMP " +
                            "(" +
                            "'countryId' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                            "'countryCode' TEXT, " +
                            "'countryName' TEXT, " +
                            "'continentId' INTEGER NOT NULL, FOREIGN KEY('continentId') REFERENCES Continent('continentId')" +
                            ")");

                    // 2. Copy the data
                    database.execSQL("INSERT INTO Country_TEMP SELECT * FROM Country");

                    // 3. Remove the old table
                    database.execSQL("DROP TABLE Country");

                    // 4. Change the table name to the correct one
                    database.execSQL("ALTER TABLE Country_TEMP RENAME TO Country");
                }
            };

            instance = Room.databaseBuilder(context, AppDatabase.class, SQLiteDB.DATABASE_NAME).
                    addMigrations(MIGRATION_1_2).build();
        }

        return instance;
    }

    public static void destroyInstance(){
        instance = null;
    }

    public abstract ICountryDao countryDao();
    public abstract IContinentDao continentDao();
}
