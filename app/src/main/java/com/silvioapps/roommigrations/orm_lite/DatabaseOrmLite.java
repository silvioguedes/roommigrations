package com.silvioapps.roommigrations.orm_lite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.silvioapps.roommigrations.models.Continent;
import com.silvioapps.roommigrations.models.Country;
import com.silvioapps.roommigrations.sqlite.SQLiteDB;
import java.sql.SQLException;

public class DatabaseOrmLite extends OrmLiteSqliteOpenHelper {
    public DatabaseOrmLite(Context context) {
        super(context, SQLiteDB.DATABASE_NAME, null, SQLiteDB.SQLITE_VERSION);
    }

    public static ConnectionSource getConnectionSource(Context context) {
        return new DatabaseOrmLite(context).getConnectionSource();
    }

    public void createTables(ConnectionSource connectionSource) throws SQLException {
        TableUtils.createTable(connectionSource, Country.class);
        TableUtils.createTable(connectionSource, Continent.class);
    }

    public void dropTables(ConnectionSource connectionSource) throws SQLException {
        TableUtils.dropTable(connectionSource, Country.class, true);
        TableUtils.dropTable(connectionSource, Continent.class, true);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            this.createTables(connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldDbVersion, int newDbVersion) {
        try {
            this.dropTables(connectionSource);
            onCreate(db, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
