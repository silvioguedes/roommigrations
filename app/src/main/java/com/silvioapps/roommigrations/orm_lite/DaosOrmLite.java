package com.silvioapps.roommigrations.orm_lite;

import android.content.Context;
import android.util.Log;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.silvioapps.roommigrations.models.Continent;
import com.silvioapps.roommigrations.models.Country;

public class DaosOrmLite {
    private static DaosOrmLite instance = null;
    private static ConnectionSource connectionSource;

    private DaosOrmLite(ConnectionSource connectionSource){
        DaosOrmLite.connectionSource = connectionSource;
    }

    public static DaosOrmLite getInstance(Context context){
        if(instance == null){
            instance = new DaosOrmLite(connectionSource);
            connectionSource = DatabaseOrmLite.getConnectionSource(context);
        }

        return instance;
    }

    public ConnectionSource getConnectionSource(){
        return connectionSource;
    }

    public static void destroyInstance(){
        instance = null;
    }

    public Dao<Continent, Long> getContinentsDao(){
        Dao<Continent, Long> dao = null;
        try {
            dao = DaoManager.createDao(connectionSource, Continent.class);
        } catch (Exception e) {
            Log.i("TAG***", e.getMessage());
        }
        return dao;
    }

    public Dao<Country, Long> getCountriesDao(){
        Dao<Country, Long> dao = null;
        try {
            dao = DaoManager.createDao(connectionSource, Country.class);
        } catch (Exception e) {
            Log.i("TAG***", e.getMessage());
        }
        return dao;
    }
}