package com.silvioapps.roommigrations.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.silvioapps.roommigrations.models.Continent;
import com.silvioapps.roommigrations.models.ContinentWithCountries;
import com.silvioapps.roommigrations.models.Country;
import java.util.ArrayList;
import java.util.List;

public class SQLiteDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "room_migrations.db";
    public static final String COUNTRY_TABLE = "Country";
    public static final String CONTINENT_TABLE = "Continent";
    public static final String COUNTRY_ID = "countryId";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String COUNTRY_NAME = "countryName";
    public static final String CONTINENT_ID = "continentId";
    public static final String CONTINENT_CODE = "continentCode";
    public static final String CONTINENT_NAME = "continentName";
    public static final int SQLITE_VERSION = 23;
    private static SQLiteDatabase sqLiteDatabase = null;

    public SQLiteDB(Context context) {
        super(context, DATABASE_NAME, null, SQLITE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + CONTINENT_TABLE + "("
                + CONTINENT_ID + " integer primary key autoincrement, "
                + CONTINENT_CODE + " text, "
                + CONTINENT_NAME + " text " + ")";

        db.execSQL(sql);

        sql = "CREATE TABLE " + COUNTRY_TABLE + "("
                + COUNTRY_ID + " integer primary key autoincrement, "
                + COUNTRY_CODE + " text, "
                + COUNTRY_NAME + " text "
                + ", " + CONTINENT_ID + " integer, "
                + " FOREIGN KEY (" + CONTINENT_ID + ") REFERENCES "+CONTINENT_TABLE+" (" + CONTINENT_ID + ") "
                + ")";

        db.execSQL(sql);
    }
  
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + COUNTRY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CONTINENT_TABLE);

        onCreate(db);
    }

    public void insert(int continentId, String continentCode, String continentName, List<Country> countryList){
        sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTINENT_ID, continentId);
        contentValues.put(CONTINENT_CODE, continentCode);
        contentValues.put(CONTINENT_NAME, continentName);

        long r = sqLiteDatabase.insert(CONTINENT_TABLE, null, contentValues);
        if (r == -1) {
            Log.e("TAG***","error continent");
        }
        else {
            Log.i("TAG***","continent success");
        }

        contentValues = new ContentValues();
        for(Country country : countryList) {
            contentValues.put(COUNTRY_ID, country.getCountryId());
            contentValues.put(COUNTRY_CODE, country.getCountryCode());
            contentValues.put(COUNTRY_NAME, country.getCountryName());
            contentValues.put(CONTINENT_ID, continentId);

            r = sqLiteDatabase.insert(COUNTRY_TABLE, null, contentValues);
            if (r == -1) {
                Log.e("TAG***","error country");
            }
            else {
                Log.i("TAG***","country success");
            }
        }

        sqLiteDatabase.close();
    }

    public List<ContinentWithCountries> query(){
        sqLiteDatabase = getWritableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + CONTINENT_TABLE, null);

        return cursorToContinentList(cursor);
    }

    public static List<ContinentWithCountries> cursorToContinentList(Cursor continentCursor){
        List<ContinentWithCountries> continentList = new ArrayList<>();
        if(continentCursor != null) {
            for (int i = 0; i < continentCursor.getCount(); i++) {
                continentCursor.moveToNext();

                int continentId = continentCursor.getInt(0);
                List<Country> countryList = new ArrayList<>();
                Cursor countryCursor = sqLiteDatabase.rawQuery("SELECT * FROM " + COUNTRY_TABLE + " WHERE " + " continentId = " +
                        continentId, null);

                if(countryCursor != null){
                    for (int j = 0; j < countryCursor.getCount(); j++) {
                        countryCursor.moveToNext();

                        Country country = new Country(countryCursor.getInt(0),countryCursor.getString(1),countryCursor.getString(2), null);
                        countryList.add(country);
                    }

                    countryCursor.close();
                }

                Continent continent = new Continent(continentId, continentCursor.getString(1),
                        continentCursor.getString(2));

                ContinentWithCountries continentWithCountries = new ContinentWithCountries(continent, countryList);

                continentList.add(continentWithCountries);
            }

            if (sqLiteDatabase != null){
                sqLiteDatabase.close();
            }

            continentCursor.close();
        }

        return continentList;
    }
}