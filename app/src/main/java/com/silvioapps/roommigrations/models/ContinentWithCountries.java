package com.silvioapps.roommigrations.models;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Relation;
import java.util.List;

/**
 * Created by silvio on 9/19/17.
 */

public class ContinentWithCountries {
    @Embedded
    private Continent continent;

    @Relation(parentColumn = "continentId", entityColumn = "continentId", entity = Country.class)
    private List<Country> countriesList;

    public ContinentWithCountries(){}

    @Ignore
    public ContinentWithCountries(Continent continent, List<Country> countriesList) {
        this.continent = continent;
        this.countriesList = countriesList;
    }

    public List<Country> getCountriesList() {
        return countriesList;
    }

    public void setCountriesList(List<Country> countriesList) {
        this.countriesList = countriesList;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }
}
