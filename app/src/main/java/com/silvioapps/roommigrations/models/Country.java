package com.silvioapps.roommigrations.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
@Entity(tableName = "Country",
        foreignKeys=@ForeignKey(
        entity=Continent.class,
        parentColumns="continentId",
        childColumns="continentId"),
        indices=@Index(value="continentId"))
public class Country{
    @PrimaryKey(autoGenerate = true)
    @DatabaseField(generatedId = true)
    private int countryId;
	@DatabaseField
	private String countryCode;
	@DatabaseField
	private String countryName;
    public int continentId;
    @Ignore
    @DatabaseField(foreign = true)
    public Continent continent;

    public Country(){}

    @Ignore
    public Country(int countryId, String countryCode, String countryName, Continent continent){
		this.countryId = countryId;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.continent = continent;
    }

	public void setCountryCode(String code){
		this.countryCode = code;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setCountryName(String countryName){
		this.countryName = countryName;
	}

	public String getCountryName(){
		return countryName;
	}

    public void setCountryId(int countryId){
        this.countryId = countryId;
    }

    public int getCountryId(){
        return countryId;
    }
}