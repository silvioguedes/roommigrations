package com.silvioapps.roommigrations.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
@Entity(tableName = "Continent")
public class Continent {
    @PrimaryKey(autoGenerate = true)
    @DatabaseField(generatedId = true)
    private int continentId;
	@DatabaseField
	private String continentCode;
	@DatabaseField
	private String continentName;
    @Ignore
    @ForeignCollectionField()
    public ForeignCollection<Country> countryList;

    public Continent(){}

    @Ignore
    public Continent(int continentId, String continentCode, String continentName){
		this.continentId = continentId;
        this.continentCode = continentCode;
        this.continentName = continentName;
    }

	public void setContinentCode(String continentCode){
		this.continentCode = continentCode;
	}

	public String getContinentCode(){
		return continentCode;
	}

	public void setContinentName(String continentName){
		this.continentName = continentName;
	}

	public String getContinentName(){
		return continentName;
	}

    public void setContinentId(int continentId){
        this.continentId = continentId;
    }

    public int getContinentId(){
        return continentId;
    }
}