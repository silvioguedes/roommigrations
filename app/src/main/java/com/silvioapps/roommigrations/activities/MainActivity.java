package com.silvioapps.roommigrations.activities;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.sql.SQLException;
import android.util.Log;

import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.CloseableWrappedIterable;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.misc.TransactionManager;
import com.silvioapps.roommigrations.models.Continent;
import com.silvioapps.roommigrations.models.ContinentWithCountries;
import com.silvioapps.roommigrations.models.Country;
import com.silvioapps.roommigrations.orm_lite.DaosOrmLite;
import com.silvioapps.roommigrations.R;
import com.silvioapps.roommigrations.room.AppDatabase;
import com.silvioapps.roommigrations.sqlite.SQLiteDB;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;//

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            //sqliteInsert();
            //ormliteInsert();

            roomQuery();
            //sqliteQuery();
            //ormliteQuery();
        }
    }

    public void sqliteInsert(){
        SQLiteDB sqLiteDB = new SQLiteDB(this);

        List<Country> countryList = new ArrayList<>();
        countryList.clear();
        countryList.add(new Country(0,"BR", "Brazil", null));
        countryList.add(new Country(1,"AR", "Argentina", null));
        countryList.add(new Country(2,"UR", "Uruguay", null));

        sqLiteDB.insert(0,"SA", "South America", countryList);

        countryList = new ArrayList<>();
        countryList.clear();
        countryList.add(new Country(3,"US", "United States", null));
        countryList.add(new Country(4,"CA", "Canada", null));
        countryList.add(new Country(5,"MX", "Mexico", null));

        sqLiteDB.insert(1,"NA", "North America", countryList);

        countryList = new ArrayList<>();
        countryList.clear();
        countryList.add(new Country(6,"UK", "United Kingdom", null));
        countryList.add(new Country(7,"GE", "Germany", null));
        countryList.add(new Country(8,"RU", "Russia", null));

        sqLiteDB.insert(2,"EU", "Europe", countryList);
    }

    public void ormliteInsert(){
        try {
            final DaosOrmLite daosOrmLite = DaosOrmLite.getInstance(this);
            TransactionManager.callInTransaction(daosOrmLite.getConnectionSource(), new Callable<Void>() {
                public Void call() throws Exception {
                    List<Continent> continentList = new ArrayList<>();

                    Continent continent = new Continent(0,"SA", "South America");
                    //daosOrmLite.getContinentsDao().create(continent);

                    List<Country> countryList = new ArrayList<>();
                    /*countryList.add(new Country(1,"BR", "Brazil", continent));
                    countryList.add(new Country(2,"AR", "Argentina", continent));
                    countryList.add(new Country(3,"UR", "Uruguay", continent));
                    daosOrmLite.getCountriesDao().create(countryList);*/
                    daosOrmLite.getCountriesDao().create(new Country(1,"BR", "Brazil", continent));

                    continentList.add(continent);

                    /*continent = new Continent(4,"NA", "North America");
                    daosOrmLite.getContinentsDao().create(continent);

                    countryList = new ArrayList<>();
                    countryList.add(new Country(5,"US", "United States", continent));
                    countryList.add(new Country(6,"CA", "Canada", continent));
                    countryList.add(new Country(7,"MX", "Mexico", continent));
                    daosOrmLite.getCountriesDao().create(countryList);

                    continentList.add(continent);

                    continent = new Continent(8,"EU", "Europe");
                    daosOrmLite.getContinentsDao().create(continent);

                    countryList = new ArrayList<>();
                    countryList.add(new Country(9,"UK", "United Kingdom", continent));
                    countryList.add(new Country(10,"GE", "Germany", continent));
                    countryList.add(new Country(11,"RU", "Russia", continent));
                    daosOrmLite.getCountriesDao().create(countryList);

                    continentList.add(continent);

                    for (Continent obj : continentList) {
                        //daosOrmLite.getContinentsDao().create(obj);
                    }*/

                    return null;
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void sqliteQuery(){
        SQLiteDB sqLiteDB = new SQLiteDB(this);
        List<ContinentWithCountries> continentList = sqLiteDB.query();
        if(continentList != null && !continentList.isEmpty()){
            for (ContinentWithCountries continentWithCountries : continentList) {
                Log.i("TAG***", "FROM SQLite getContinentId " + continentWithCountries.getContinent().getContinentId() +
                        " getCode " + continentWithCountries.getContinent().getContinentCode() + " getName " +
                        continentWithCountries.getContinent().getContinentName());

                for(Country country : continentWithCountries.getCountriesList()){
                    Log.i("TAG***", "FROM SQLite getCountryId " + country.getCountryId() + " getCode " + country.getCountryCode() +
                            " getName " + country.getCountryName());
                }
            }
        }
    }

    public void ormliteQuery(){
        try {
            List<Country> countryList = DaosOrmLite.getInstance(this).getCountriesDao().queryBuilder().query();
            for(Country country : countryList){
                Continent continent = country.continent;

                Log.i("TAG***", "FROM SQLite getCountryId " + country.getCountryId() + " getCode " + country.getCountryCode() +
                        " getName " + country.getCountryName());

                Log.i("TAG***", "FROM SQLite getContinentId " + continent.getContinentId() + " getCode " + continent.getContinentCode() +
                        " getName " + continent.getContinentName());
            }

            /*List<Continent> continentList = DaosOrmLite.getInstance(this).getContinentsDao().queryBuilder().query();
            for (Continent continent : continentList) {
                Log.i("TAG***", "FROM SQLite getContinentId " + continent.getContinentId() + " getCode " + continent.getContinentCode() +
                        " getName " + continent.getContinentName());

                for(Country country : continent.countryList){
                    Log.i("TAG***", "FROM SQLite getCountryId " + country.getCountryId() + " getCode " + country.getCountryCode() +
                            " getName " + country.getCountryName());
                }
            }*/

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void roomQuery(){
        new AsyncTask<Void, Void, Void>() {
            List<ContinentWithCountries> continentWithCountriesList = null;

            @Override
            protected void onPreExecute (){}

            @Override
            protected Void doInBackground(Void... voids) {
                continentWithCountriesList = AppDatabase.getInstance(MainActivity.this).continentDao().getAllContinentsWithCountries();

                return null;
            }

            @Override
            protected void onPostExecute(Void arg) {
                for (ContinentWithCountries continentWithCountries : continentWithCountriesList) {
                    Log.i("TAG***", "FROM SQLite getContinentId " + continentWithCountries.getContinent().getContinentId() +
                            " getCode " + continentWithCountries.getContinent().getContinentCode() +
                            " getName " + continentWithCountries.getContinent().getContinentName());

                    for(Country country : continentWithCountries.getCountriesList()){
                        Log.i("TAG***", "FROM SQLite getCountryId " + country.getCountryId() + " getCode " + country.getCountryCode() +
                                " getName " + country.getCountryName());
                    }
                }
            }
        }.execute();
    }
}
